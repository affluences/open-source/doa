import requests
from urllib3.exceptions import NewConnectionError

from util import settings
from util.logger import log
from util.settings import TunnelInfo

# global minimum configuration
CADDY_CONF_MIN = """
{
	admin 0.0.0.0:2019
	auto_https off
}
"""

# template repeated for each virtual host
CADDY_CONF_HOST = """
%domain%:80 {
	reverse_proxy %target%:%port% {
		transport http {
			keepalive off
		}
	}
}
"""


def update(conf: str):
    """
    Sends the updated configuration to the Caddy server

    :param conf: the caddyfile configuration in plain text
    """
    try:
        r = requests.post("http://%s:%s/load" % (settings.PROXY_URL, settings.PROXY_ADMIN_PORT), data=conf, headers={'Content-type': 'text/caddyfile'})
        if r.status_code == 200:
            return True
        else:
            log.debug("caddy server could not update conf, got HTTP " + str(r.status_code))
            return False
    except NewConnectionError as e:
        log.error("failed to reach the Caddy server, the config could not be updated")
        return False


def generate(hosts: [TunnelInfo]) -> str:
    """
    Generates the Caddyfile configuration from the current list of tunnels

    :param hosts: array of TunnelInfo objects representing the current list of active tunnels
    :return: the configuration in plain text
    """
    conf = CADDY_CONF_MIN
    for host in hosts:
        conf += "\n" + CADDY_CONF_HOST.replace("%domain%", host.tunnel_host).replace("%target%", host.target_host).replace("%port%", str(host.target_port))
    return conf
