from dataclasses import dataclass

from environs import Env, EnvValidationError

from util import logger
from util.logger import log

env = Env()
APP_LOG_LEVEL: str
APP_ENV: str
DOCKER_URL: str
DOCKER_REFRESH_INTERVAL: int  # seconds
PROXY_URL: str
PROXY_PORT: int
PROXY_ADMIN_PORT: int
CERT_PATH: str
TUNNEL_HOST_LABEL: str
TUNNEL_ENABLED_LABEL: str
TUNNEL_PORT_LABEL: str
TUNNEL_PROTO_LABEL: str
TUNNEL_SKIP_NETWORK_VALIDATION_LABEL: str
TUNNEL_HOST_SUFFIX: str
TUNNEL_NETWORK: str


@dataclass
class TunnelInfo:
    tunnel_host: str
    target_host: str
    target_port: int
    target_proto: str


def init():
    global APP_LOG_LEVEL, APP_ENV, DOCKER_URL, DOCKER_REFRESH_INTERVAL, PROXY_URL, PROXY_PORT, PROXY_ADMIN_PORT, CERT_PATH, TUNNEL_HOST_LABEL, TUNNEL_HOST_SUFFIX, TUNNEL_ENABLED_LABEL, TUNNEL_PORT_LABEL, TUNNEL_NETWORK, TUNNEL_PROTO_LABEL, TUNNEL_SKIP_NETWORK_VALIDATION_LABEL
    try:
        APP_ENV = env.str("APP_ENV", "prod", validate=lambda n: n in ['debug', 'prod', 'test'])
        if APP_ENV == "debug" or APP_ENV == "test":
            APP_LOG_LEVEL = "debug"
        else:
            APP_LOG_LEVEL = env.str("APP_LOG_LEVEL", "info", validate=lambda n: n in ['debug', 'info', 'notice', 'warning', 'error', 'critical'])
        DOCKER_URL = env.str("DOCKER_URL", "unix://var/run/docker.sock")
        DOCKER_REFRESH_INTERVAL = env.int("DOCKER_REFRESH_INTERVAL", 10)
        PROXY_URL = env.str("PROXY_URL", "argo-proxy")
        PROXY_PORT = env.int("PROXY_PORT", 80)
        PROXY_ADMIN_PORT = env.int("PROXY_ADMIN_PORT", 2019)
        CERT_PATH = env.str("CERT_PATH", "/certs")
        TUNNEL_HOST_LABEL = env.str("TUNNEL_HOST_LABEL", "cloudflare.tunnel.host")
        TUNNEL_HOST_SUFFIX = env.str("TUNNEL_HOST_SUFFIX", "example.com")
        TUNNEL_ENABLED_LABEL = env.str("TUNNEL_ENABLED_LABEL", "cloudflare.tunnel.enabled")
        TUNNEL_PORT_LABEL = env.str("TUNNEL_PORT_LABEL", "cloudflare.tunnel.port")
        TUNNEL_PROTO_LABEL = env.str("TUNNEL_PROTO_LABEL", "cloudflare.tunnel.proto")
        TUNNEL_NETWORK = env.str("TUNNEL_NETWORK", "cloudflare")
        TUNNEL_SKIP_NETWORK_VALIDATION_LABEL = env.str("TUNNEL_SKIP_NETWORK_VALIDATION_LABEL", "cloudflare.tunnel.skip_network_validation")
        logger.currentLevel = APP_LOG_LEVEL
    except EnvValidationError as e:
        log.error('Invalid or missing environment variables', exception=e)
        exit(1)
