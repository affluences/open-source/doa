import time
from typing import Optional

import docker
import requests
from docker.errors import APIError
from docker.models.services import Service

from util import settings
from util.logger import log


class Swarm:

    def __init__(self, base_url: str, max_retries: int = 10, retry_delay: int = 5):
        """

        :param base_url: the url to join the engine. examples : unix://var/run/docker.sock or tcp://127.0.0.1:8080
        :param max_retries: max number of retries in case connection fails
        :param retry_delay: amount of time to wait between retries in seconds
        """
        self.__base_url = base_url
        self.__max_retries = max_retries
        self.__retry_delay = retry_delay
        self.__client = None

    def connect(self) -> bool:
        """
        Connects to a Docker Engine with a retry strategy

        :return: returns the client instance if successful, false otherwise
        """
        for i in range(self.__max_retries):
            ping = None
            client = docker.DockerClient(base_url=self.__base_url)
            try:
                ping = client.ping()
            except (requests.exceptions.ConnectionError, APIError):
                ping = False
            finally:
                if ping:
                    self.__client = client
                    return True
                else:
                    log.warning("failed to connect to Docker Engine. retrying in " + str(self.__retry_delay) + " seconds")
                    time.sleep(self.__retry_delay)
        return False

    def services(self):
        return self.__client.services.list(filters={"label": [settings.TUNNEL_ENABLED_LABEL, settings.TUNNEL_HOST_LABEL]})


def strip_stackname(name: str):
    """
    Removes the stack name from a swarm scoped service

    :param name: service name
    :return: service name without the stack name
    """
    return name.split("_")[-1]


def label(service: Service, name: str, default=False) -> Optional[str]:
    """
    Fetches a label from a Swarm service

    :param service: service obj
    :param name: name of the label
    :param default: default value to return if the label is not found
    :return: label's value or default value or None
    """
    if name in service.attrs['Spec']['Labels']:
        if isinstance(service.attrs['Spec']['Labels'][name], str):
            return service.attrs['Spec']['Labels'][name]
        else:
            return str(service.attrs['Spec']['Labels'][name])
    if default is not False:
        return str(default)
    else:
        return None


def network_reachable(service: Service, network: str) -> bool:
    """
    Checks if a service is reachable on a given Docker Network

    :param service: docker service obj
    :param network: network name
    :return: true if found on the network, false otherwise
    """
    try:
        for task in service.tasks():
            if task['Status']['State'] == "running":
                for net in task['NetworksAttachments']:
                    net_name = net['Network']['Spec']['Name']
                    if "_" in net_name:
                        net_name = strip_stackname(net_name)
                    if net_name == network:
                        return True
    except:
        return False
