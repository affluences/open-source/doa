import io
import os
import sys
import threading
from datetime import datetime

from structlog import PrintLogger, wrap_logger, DropEvent

levels = {
    'debug': 1,
    'info': 2,
    'notice': 3,
    'warning': 4,
    'error': 5,
    'critical': 6
}

currentLevel = 'debug'


def _repr(val) -> str:
    """
    Returns the representation of a value as a string

    :param val: any value
    :return: string representation of val
    """
    if isinstance(val, str):
        return val
    else:
        return repr(val)


def _sanitize_level(level: str) -> str:
    """
    Normalizes log level from non-standards log level names

    :param level: log level
    :return: proper level
    """
    if level == 'fatal' or level == 'failure':
        return 'critical'
    if level == 'exception' or level == 'err':
        return 'error'
    if level == 'warn':
        return 'warning'
    if level not in ['debug', 'info', 'notice', 'warning', 'error', 'critical']:
        return 'info'
    else:
        return level.lower()


def _check_level(level) -> bool:
    """
    Checks if the input level is allowed the app's current minimum log level

    :param level: log level to check
    :return: true if allowed, false otherwise
    """
    return levels[level] >= levels[currentLevel]


def _sanitize_var(obj) -> str:
    """
    escapes line breaks to avoid breaking the log format

    :param obj: value to sanitize
    :return: sanitized value as string
    """
    return _repr(obj).replace('"', '\\"').replace("\n", "\\n")


def _logfmt_processor(_, log_method, event_dict):
    """
    Structlog processor to handle & format logs using the logfmt format

    :param _:
    :param log_method:
    :param event_dict:
    :return:
    """
    level = _sanitize_level(log_method)
    if not _check_level(level):
        raise DropEvent
    sio = io.StringIO()
    # remove reserved keys in case they have been hijacked
    event_dict.pop("time", None)
    event_dict.pop("level", None)
    event_dict.pop("msg", None)

    # inject reserved keys
    time = datetime.utcnow().isoformat(timespec='milliseconds') + 'Z'
    msg = _sanitize_var(event_dict.pop("event", "missing msg field"))
    trace = event_dict.pop("exception", None)
    exc_info = event_dict.pop("exc_info", None)
    sio.write("time=\"" + time + "\"")
    sio.write(" " + "level=\"" + level + "\"")
    sio.write(" " + "msg=\"" + msg + "\"")
    if threading.current_thread() is not threading.main_thread():
        sio.write(" " + "thread=\"" + str(threading.get_ident()) + "\"")
    if trace is not None:
        sio.write(" error_message=\"" + str(trace) + "\"")
        sio.write(" error_class=\"" + sys.exc_info()[0].__name__ + "\"")
        sio.write(" file=\"" + os.path.basename(sys.exc_info()[2].tb_frame.f_code.co_filename) + "\"")
        sio.write(" line=\"" + str(sys.exc_info()[2].tb_lineno) + "\"")
        if hasattr(trace, 'extras'):
            for key, value in trace.extras.items():
                event_dict[key] = value
    if exc_info is not None:
        sio.write(" error_message=\"" + str(exc_info[1]) + "\"")
        # going up the stack to find where things broke
        tb = exc_info[2]
        while 1:
            if not tb.tb_next:
                break
            tb = tb.tb_next
        sio.write(" error_class=\"" + exc_info[0].__name__ + "\"")
        sio.write(" file=\"" + os.path.basename(tb.tb_frame.f_code.co_filename) + "\"")
        sio.write(" line=\"" + str(tb.tb_lineno) + "\"")
    sio.write(" " +
              " ".join(
                  key
                  + "="
                  + "\"" + _sanitize_var(event_dict[key]) + "\""
                  for key in sorted(event_dict.keys())
              )
              )
    return sio.getvalue()


log = wrap_logger(PrintLogger(), processors=[_logfmt_processor], context_class=dict)


def _handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    log.critical('an uncaught exception occurred : aborting !', exc_info=(exc_type, exc_value, exc_traceback))
    exit(1)


def init(global_handler: bool = False):
    if global_handler:
        sys.excepthook = _handle_exception
