from doa import DoA
from util import logger
from util import settings
from util.logger import log
from version import __version__


def main():
    logger.init(global_handler=True)
    settings.init()
    log.info("starting Docker-Over-Argo version %s" % __version__)
    log.info("app running in '%s' mode and using the '%s' log level" % (settings.APP_ENV, settings.APP_LOG_LEVEL))
    DoA()
    log.info("shutting down Docker-Over-Argo version %s, bye!" % __version__)


if __name__ == "__main__":
    main()
