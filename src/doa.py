import threading
from signal import signal, SIGINT, SIGTERM

from tunnel import Tunnel
from util import settings, swarm, caddy
from util.logger import log
from util.settings import TunnelInfo
from util.swarm import Swarm


class DoA:
    def __init__(self):
        signal(SIGINT, self.__signal_handler)
        signal(SIGTERM, self.__signal_handler)
        self.__exit = threading.Event()
        self.__tunnels = []
        self.__tunnelsMutex = threading.Lock()
        self.__docker = Swarm(base_url=settings.DOCKER_URL)
        if not self.__docker.connect():
            raise RuntimeError("Could not connect to docker daemon")
        self.__running = True
        self.watch()
        self.shutdown()

    def watch(self):
        requires_update = False
        while not self.__exit.is_set():
            candidates = self.find_candidates()

            # shut down no longer needed tunnels & update existing tunnels in case of service/port change
            for t in self.__tunnels:
                candidate = next((x for x in candidates if x.tunnel_host == t.info.tunnel_host), None)
                if candidate is None:
                    self.unregister_tunnel(t.info)
                    requires_update = True
                elif t.info.target_host != candidate.target_host \
                        or t.info.target_port != candidate.target_port \
                        or t.info.target_proto != candidate.target_proto:
                    t.info = candidate
                    requires_update = True

            # create new tunnels
            for c in candidates:
                if next((x for x in self.__tunnels if x.info.tunnel_host == c.tunnel_host), None) is None:
                    self.register_tunnel(c)
                    requires_update = True

            # push config update if required
            if requires_update:
                self.update_proxy()
                self.announce()
                requires_update = False
            self.__exit.wait(settings.DOCKER_REFRESH_INTERVAL)

    def find_candidates(self) -> [TunnelInfo]:
        """
        Fetches Docker Swarm services that matches the required labels and build a list of TunnelInfo objects

        :return: array of TunnelInfo objects
        """
        candidates = []
        for s in self.__docker.services():
            enabled = swarm.label(s, settings.TUNNEL_ENABLED_LABEL)
            host = swarm.label(s, settings.TUNNEL_HOST_LABEL)
            port = swarm.label(s, settings.TUNNEL_PORT_LABEL)
            proto = swarm.label(s, settings.TUNNEL_PROTO_LABEL, "http")
            skip_net = swarm.label(s, settings.TUNNEL_SKIP_NETWORK_VALIDATION_LABEL, "false")
            if enabled == "true":
                if host is None:
                    log.error("invalid host for %s" % s.name)
                elif port is None:
                    log.error("invalid port for %s" % s.name)
                elif not port.isdigit():
                    log.error("invalid port for %s" % s.name)
                elif skip_net == "false" and not swarm.network_reachable(s, settings.TUNNEL_NETWORK):
                    log.error("service %s is unreachable" % s.name)
                elif proto not in ["http", "tcp"]:
                    log.error("invalid protocol for %s (must be http or tcp)" % s.name)
                else:
                    target = s.name
                    candidate = TunnelInfo("%s.%s" % (host, settings.TUNNEL_HOST_SUFFIX), target, int(port), proto)
                    candidates.append(candidate)
        return candidates

    def register_tunnel(self, info: TunnelInfo):
        log.debug("registering new tunnel for " + info.tunnel_host)
        t = Tunnel(info=info, manager=self)
        self.__tunnelsMutex.acquire()
        self.__tunnels.append(t)
        self.__tunnelsMutex.release()
        t.start()

    def unregister_tunnel(self, info: TunnelInfo):
        log.debug("unregistering tunnel for " + info.tunnel_host)
        t = next((x for x in self.__tunnels if x.info.tunnel_host == info.tunnel_host), None)
        if t is not None:
            t.stop()
        else:
            log.error("could not unregister tunnel for hostname %s, it doesn't exist!" % info.tunnel_host)

    def on_tunnel_terminated(self, info: TunnelInfo):
        log.debug("termination callback received for tunnel " + info.tunnel_host)
        self.__tunnelsMutex.acquire()
        t = next((x for x in self.__tunnels if x.info.tunnel_host == info.tunnel_host), None)
        if t is not None:
            self.__tunnels.remove(t)
        self.__tunnelsMutex.release()

    def update_proxy(self):
        entries = []
        for t in self.__tunnels:
            if t.info.target_proto == "http":
                entries.append(t.info)
        conf = caddy.generate(entries)
        caddy.update(conf)

    def announce(self):
        for t in self.__tunnels:
            if t.isActive:
                log.info("active tunnel %s pointing to %s:%d" % (t.info.tunnel_host, t.info.target_host, t.info.target_port))

    def shutdown(self):
        for t in self.__tunnels:
            t.stop()
        for t in self.__tunnels:
            t.join()

    def __signal_handler(self, signal_received, frame):
        self.__exit.set()
