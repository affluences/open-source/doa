import shlex
import subprocess
import threading
from threading import Thread

import tldextract

from util import settings
from util.logger import log
from util.settings import TunnelInfo


def read_process_output(proc):
    for line in iter(proc.stdout.readline, b''):
        log.debug("cloudflared: " + line.decode('utf-8'))


class Tunnel(Thread):
    def __init__(self, manager, info: TunnelInfo):
        Thread.__init__(self)
        self.manager = manager
        self.info = info
        self.isActive = True
        self.__subprocess = None
        self.__stop_signal = threading.Event()

    def run(self):
        """
        Starts the tunnel
        """
        log.info("starting new tunnel", hostname=self.info.tunnel_host, proto=self.info.target_proto)
        self.open_tunnel()

    def stop(self):
        """
        Stops the tunnel
        """
        self.__subprocess.terminate()
        self.__stop_signal.set()
        self.isActive = False

    def get_cmd(self) -> str:
        """
        Returns a formatted cloudflared command based on the TunnelInfo data

        :return: command as a string
        """
        domain = tldextract.extract(self.info.tunnel_host)
        certificate = "%s/%s.%s.pem" % (settings.CERT_PATH, domain.domain, domain.suffix)
        if self.info.target_proto == "tcp":
            destination_host = self.info.target_host
            destination_port = self.info.target_port
        else:
            destination_host = settings.PROXY_URL
            destination_port = settings.PROXY_PORT
        return "/usr/local/bin/cloudflared tunnel --name %s --cred-file /tunnels/%s.json --url %s://%s:%d --hostname %s --origincert %s --retries 8 --no-autoupdate" % (
            self.info.tunnel_host, self.info.tunnel_host, self.info.target_proto, destination_host, destination_port, self.info.tunnel_host, certificate)

    def open_tunnel(self):
        """
        Runs the cloudflared subprocess and listens to its output until an exit signal is received
        """
        cmd = self.get_cmd()
        log.debug("invoking cmd %s" % cmd)
        self.__subprocess = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        while self.isActive:
            output = self.__subprocess.stdout.readline()
            if output == '' and self.__subprocess.poll() is not None:
                break
            if output:
                log.debug("cloudflared: " + str(output.strip().decode('utf-8')))
        try:
            code = self.__subprocess.wait(35)
            log.debug("subprocess exited with status " + str(code))
        except subprocess.TimeoutExpired:
            self.__subprocess.kill()
            log.debug('subprocess took too long to terminate and was killed')
        log.info("terminating tunnel", hostname=self.info.tunnel_host)
        self.manager.on_tunnel_terminated(info=self.info)

