# Docker Over Argo

Docker Over Argo is a utility made to automatically open and close Cloudflare Argo tunnels and connect them to HTTP & TCP based Docker Swarm services.

## How it works

DOA watches the running services using the Docker Engine's API to find the services that needs to be tunneled. It checks for a set of labels to determinate what service to connect to Argo along with the port and the subdomain to use. Tunnels are opened and closed when a container starts or stops. For instance if a service is removed or is scaled down to 0 replicas then the tunnel gets closed automatically.

Tunnels are not directly connected to the each containers (except for TCP containers) to avoid the need to open a new tunnel for each replica of a service (CF has a hard 1000 limit) and the need to use Cloudflare's load-balancer. Instead it relies on a sidecar Caddy (2.1.1+) reverse-proxy to forward the request to the service and let Swarm's native load balancing do the rest !

When a tunnel is opened, DOA updates Caddy's configuration so it knows where to send the traffic.

## Running Docker Over Argo

### Getting started 

- Get a copy of [the certificate](https://dash.cloudflare.com/warp) (pem file) of the domain the tunnel is bound to
- Get a copy of the Caddyfile inside the example directory
- Create a dedicated overlay network for DOA, the Caddy reverse proxy and the services you want to connect
- Have access to the docker engine daemon (usually unix://var/run/docker.sock)

With all that, take a look at the included example docker-compose.yml in the example directory to get started.


### Services labels

To connect a service to an Argo tunnel simply provide the following service labels : 

- `cloudflare.tunnel.enabled` = true
- `cloudflare.tunnel.host` = subdomain (e.g. "admin" would create a tunnel for "admin.yourdomain.tld")
- `cloudflare.tunnel.port`= port your service listens on inside the container (e.g. "80")
- `cloudflare.tunnel.proto`= protocol (http or tcp. Default: http)

The labels needs to be placed in the `deploy` block of your stack and not on the container itself.

The name of those labels can be overridden by environment variables as described below.

### Environment variables

Use those to configure Docker Over Argo

| var                     | default                    | description                                                                                                   |
|-------------------------|----------------------------|---------------------------------------------------------------------------------------------------------------|
| APP\_ENV                 | prod                       | allowed values are prod, debug, test                                                                          |
| APP\_LOG\_LEVEL           | info                       | allowed values are debug, info, notice, warning, error, critical                                              |
| DOCKER_URL              | unix://var/run/docker.sock | Docker Engine url                                                                                             |
| DOCKER\_REFRESH\_INTERVAL | 10                         | time in seconds between swarm service lookups                                                                 |
| PROXY\_URL               | argo-proxy          | url of the caddy server                                                                                       |
| PROXY\_PORT              | 80                         | port of the caddy server                                                                                      |
| PROXY\_ADMIN_PORT        | 2019                       | admin port of the caddy server (api)                                                                          |
| CERT\_PATH               | /certs                     | path where to look for certificates                                                                           |
| TUNNEL\_HOST\_LABEL       | cloudflare.tunnel.host     | label name to look for. It's the subdomain attached to the service                                            |
| TUNNEL\_PORT\_LABEL       | cloudflare.tunnel.port     | label name to look for. It's the port the service listens on inside the containers                            |
| TUNNEL\_PROTO\_LABEL       | cloudflare.tunnel.proto     | label name to look for. It's the protocol the service uses                          |
| TUNNEL\_ENABLED\_LABEL    | cloudflare.tunnel.enabled  | label name to look for. It determines if the service has to be exposed (services are not exposed by default)  |
| TUNNEL\_SKIP\_NETWORK\_VALIDATION\_LABEL    | cloudflare.tunnel.skip_network_validation  | label name to look for. It determines if DOA will check if there is a route to this service |
| TUNNEL\_HOST\_SUFFIX      | example.com                |                                                                                                               |
| TUNNEL\_NETWORK          | cloudflare                 | name of the Docker network the reverse proxy will use to connect to the services. Used to check reachability. |