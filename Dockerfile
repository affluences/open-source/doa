FROM python:3.8.3-slim-buster
LABEL MAINTAINER="Raphaël Galmiche <raphael.galmiche@affluences.com>"

RUN export DEBIAN_FRONTEND=noninteractive && apt-get update && apt-get install -y --no-install-recommends curl ca-certificates && rm -rf /var/lib/apt/lists/*
RUN curl -L -o /usr/local/bin/cloudflared https://github.com/cloudflare/cloudflared/releases/download/2022.11.0/cloudflared-linux-amd64
RUN chmod +x /usr/local/bin/cloudflared

COPY requirements.txt /app/
WORKDIR /app
RUN pip install -r requirements.txt
COPY ./src/ /app/

VOLUME /tunnels

# -u = unbuffered output
ENTRYPOINT ["python", "-u", "main.py"]
