# 1.2.4

### Features

- Updated the cloudflared binary to 2022.11.0

# 1.2.3

### Features

- Updated the cloudflared binary to 2022.5.0

# 1.2.2

### Features

- Updated the cloudflared binary to 2022.2.1 which brings support for named / long lasting tunnels

### Breaking changes

- As of this version DOA requires a volume mounted on /tunnels to persist the tunnel credentials

# 1.2.1

### Bugfixes

- Fixed unnecessary stack name stripping causing potential routing issues with the reverse proxy

# 1.2.0

### Features

- Added option to disable network validation
- Added new `cloudflare.tunnel.skip_network_validation` docker label and `TUNNEL_SKIP_NETWORK_VALIDATION_LABEL` environment variable

# 1.1.0

### Features

- Added support for TCP services
- Added new `cloudflare.tunnel.proto` docker label and `TUNNEL_PROTO_LABEL` environment variable

# 1.0.0

### Bugfixes

- fixed shutdown routine to properly close tunnels when the main thread is shutting down

### Features

- added TUNNEL_NETWORK viariable to specify the network the services needs to be reachable on
- DOA will no longer create tunnels for services that don't have at least 1 task with RUNNING state
- DOA will no longer attempt to open tunnels when the destination is not reachable through a Docker Network
- When an exposed service is disconnected from a network and no longer reachable, the associated tunnel will be closed

# 0.2.0

### Bugfixes

- fixed issue causing tunnels to be closed when destination port changed (not necessary)
- fixed issue caused by cloudflared making the tunnel threads hang forever

### Features

- added caddy config generator
- added support for custom service port
- added support for custom url

# 0.1.0

### Features

- initial release
