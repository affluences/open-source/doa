import unittest

import requests_mock

from util import caddy, settings


class TestCaddy(unittest.TestCase):

    @requests_mock.Mocker()
    def test_update_ok(self, m):
        settings.init()
        m.post('http://%s:%s/load' % (settings.PROXY_URL, settings.PROXY_ADMIN_PORT), text='', status_code=200)
        result = caddy.update("{}")
        self.assertEqual(result, True)

    @requests_mock.Mocker()
    def test_update_error(self, m):
        settings.init()
        m.post('http://%s:%s/load' % (settings.PROXY_URL, settings.PROXY_ADMIN_PORT), text='', status_code=403)
        result = caddy.update("{}")
        self.assertEqual(result, False)

    def test_update_network_error(self):
        settings.init()
        settings.PROXY_URL = "http://invalid-host"
        settings.PROXY_ADMIN_PORT = "9999"
        with self.assertRaises(Exception):
            caddy.update("{}")
