import unittest
from unittest.mock import patch

from docker.models.services import Service

from util import logger, swarm, settings


class TestSwarm(unittest.TestCase):

    def test_strip_stackname(self):
        self.assertEqual(swarm.strip_stackname("stack_service"), "service")
        self.assertEqual(swarm.strip_stackname("service"), "service")

    def test_label(self):
        settings.init()
        mock_service = Service()
        # no label & no default case
        mock_service.attrs = {
            "Spec": {
                "Labels": {}
            }
        }
        self.assertIsNone(swarm.label(mock_service, settings.TUNNEL_ENABLED_LABEL))
        self.assertIsNone(swarm.label(mock_service, settings.TUNNEL_PORT_LABEL))
        self.assertIsNone(swarm.label(mock_service, settings.TUNNEL_HOST_LABEL))
        # labels found & properly filled case
        mock_service.attrs = {
            "Spec": {
                "Labels": {
                    settings.TUNNEL_ENABLED_LABEL: "true",
                    settings.TUNNEL_PORT_LABEL: "80",
                    settings.TUNNEL_HOST_LABEL: "myapp"
                }
            }
        }
        self.assertEqual(swarm.label(mock_service, settings.TUNNEL_ENABLED_LABEL), "true")
        self.assertEqual(swarm.label(mock_service, settings.TUNNEL_PORT_LABEL), "80")
        self.assertEqual(swarm.label(mock_service, settings.TUNNEL_HOST_LABEL), "myapp")
        # default cases
        mock_service.attrs = {
            "Spec": {
                "Labels": {}
            }
        }
        self.assertEqual(swarm.label(mock_service, settings.TUNNEL_ENABLED_LABEL, "false"), "false")
        self.assertEqual(swarm.label(mock_service, settings.TUNNEL_PORT_LABEL, "9000"), "9000")
        self.assertEqual(swarm.label(mock_service, settings.TUNNEL_HOST_LABEL, "default-app"), "default-app")

    @patch("docker.models.services.Service")
    def test_network_reachable(self, mock_service_class):
        mock_service = mock_service_class.return_value
        mock_service.tasks.return_value = [{
            "Status": {
                "State": "running"
            },
            "NetworksAttachments": [
                {
                    "Network": {
                        "Spec": {
                            "Name": "stack_my-net"
                        }
                    }
                }
            ]
        }]
        self.assertTrue(swarm.network_reachable(mock_service, "my-net"))
        self.assertFalse(swarm.network_reachable(mock_service, "wong-net"))