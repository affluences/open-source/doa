import unittest
from unittest.mock import patch

from util import settings


class TestSettings(unittest.TestCase):

    def test_invalid_app_log_level(self):
        with patch.dict('os.environ', {'APP_LOG_LEVEL': 'super critical'}):
            with self.assertRaises(SystemExit):
                settings.init()
