import unittest

from util import logger


class TestLogger(unittest.TestCase):

    def test__repr(self):
        self.assertEqual(logger._repr(100), '100')
        self.assertEqual(logger._repr(1.3), '1.3')
        self.assertEqual(logger._repr('abcd'), 'abcd')

    def test__sanitize_level(self):
        self.assertTrue(logger._sanitize_level('fatal') == 'critical')
        self.assertTrue(logger._sanitize_level('failure') == 'critical')
        self.assertTrue(logger._sanitize_level('exception') == 'error')
        self.assertTrue(logger._sanitize_level('err') == 'error')
        self.assertTrue(logger._sanitize_level('warn') == 'warning')
        self.assertTrue(logger._sanitize_level('wrong_code') == 'info')
        self.assertTrue(logger._sanitize_level('debug') == 'debug')
        self.assertTrue(logger._sanitize_level('info') == 'info')
        self.assertTrue(logger._sanitize_level('notice') == 'notice')
        self.assertTrue(logger._sanitize_level('warning') == 'warning')
        self.assertTrue(logger._sanitize_level('error') == 'error')
        self.assertTrue(logger._sanitize_level('critical') == 'critical')

    def test__check_level(self):
        logger.currentLevel = "info"
        self.assertTrue(logger._check_level('critical'))
        self.assertTrue(logger._check_level('info'))
        self.assertFalse(logger._check_level('debug'))

    def test__sanitize_var(self):
        self.assertTrue('\n' not in logger._sanitize_var('abc\ndef'))
